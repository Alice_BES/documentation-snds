# MA_REM_FT


## Modèle de données

|Nom|Type|Description|Exemple|Propriétés|
|-|-|-|-|-|
|PRE_INS_DPT|chaîne de caractères|Département du professionnel de santé prescripteur|||
|FLX_DIS_DTD|date|Date de mise à disposition des données|||
|ORG_CLE_NUM|chaîne de caractères|organisme de liquidation des prestations (avant fusion des caisses)|||
|DDP_COD|nombre réel|Code de la discipline de Prestations (ou discipline médico tarifaire)|||
|ETP_CAT_COD|nombre réel|Catégorie d'établissement prescripteur|||
|FLT_DEP_MNT|nombre réel|Montant du dépassement de la prestation|||
|PRS_REM_MNT|nombre réel|Montant remboursé|||
|PSE_STJ_COD|nombre réel|Mode d'exercice du professionnel de santé exécutant|||
|MTM_NAT|nombre réel|Modulation du ticket modérateur|||
|PSE_CNV_COD|nombre réel|Code convention du professionnel de santé exécutant|||
|EXF_ANN_MOI|chaîne de caractères|Année Mois fin de soin exécution|||
|AGE_BEN_COD|nombre réel|Age du bénéficiaire au moment des Soins|||
|EXO_MTF|nombre réel|Motif d'exonération du ticket modérateur|||
|PSP_STJ_COD|nombre réel|Mode d'exercice du professionnel de santé prescripteur|||
|PSP_CAT_COD|nombre réel|Catégorie du PS prescripteur|||
|TOP_PS5_TRG|nombre réel|Top PS5 Tous Régimes|||
|FLT_REM_MNT|nombre réel|Montant versé - remboursé (Part de base uniquement)|||
|ETE_IND_TAA|nombre réel|Indicateur TAA|||
|PRS_REM_TAU|nombre réel|Taux de remboursement réel (régime obligatoire)|||
|PRS_NAT|nombre réel|Nature de prestation|||
|MFT_COD|nombre réel|Code du mode de fixation des tarifs|||
|ETP_STJ_COD|nombre réel|Statut juridique de l'établissement prescripteur|||
|PSE_SPE_COD|nombre réel|Spécialite médicale du professionnel de santé exécutant|||
|DRG_AFF_NAT|chaîne de caractères|Code affiné du destinataire du règlement|||
|FLT_ACT_NBR|nombre réel|Dénombrement de la prestation|||
|BEN_SEX_COD|nombre réel|Code sexe du bénéficiaire|||
|DRT_ACS_TOP|chaîne de caractères|Top bénéficiaire ACS|||
|MDT_COD|nombre réel|Code du mode de traitement|||
|MUT_ORG|nombre réel|N° SLM d'affiliation|||
|PRS_ACT_COG|nombre réel|Coefficient global de l'acte de base|||
|FLT_PAI_MNT|nombre réel|Montant de la dépense de la prestation|||
|RGM_COD|nombre réel|Code du petit régime|||
|PRS_REM_BSE|nombre réel|Base de remboursement|||
|PRS_NAT_REF|nombre réel|Code de la Prestations de référence|||
|PSP_SPE_COD|nombre réel|Spécialité médicale du professionnel de santé prescripteur|||
|BEN_CMU_TOP|chaîne de caractères|Top bénéficiaire de la CMU complémentaire|||
|PSE_ACT_NAT|nombre réel|Nature d'activité du professionnel de santé exécutant|||
|EXE_INS_DPT|chaîne de caractères|Département du professionnel de santé exécutant|||
|ORG_CAI_NUM|chaîne de caractères|N° Caisse de liquidation pour le RG|||
|FLT_ACT_QTE|nombre réel|Quantité de la prestation|||
|BEN_CMU_CAT|chaîne de caractères|Catégorie d'organisme complémentaire CMU|||
|PRS_PDS_QCP|nombre réel|Code qualificatif du parcours de soins|||
|ETE_CAT_COD|nombre réel|Catégorie de l'établissement exécutant|||
|REM_ANN_MOI|chaîne de caractères|Année Mois de remboursement|||
|ETP_DPT_COD|chaîne de caractères|Departement de l'établissement prescripteur|||
|ETE_STJ_COD|nombre réel|Statut Juridique de l'établissement juridique|||
|FLT_ACT_COG|nombre réel|Coefficient Global de la prestation|||
|ASU_NAT|nombre réel|Nature de l'assurance|||
|PRS_ACT_QTE|nombre réel|Quantité de l'acte de base|||
|EXD_ANN_MOI|chaîne de caractères|Année Mois début de soin exécution|||
|SOI_ANN|chaîne de caractères|Année de soin PS5|||
|ORG_AFF_BEN|chaîne de caractères|Code de l'organisme d'affiliation|||
|CPT_ENV_TYP|nombre réel|Type d'enveloppe|||
|RGM_GRG_COD|nombre réel|Grand régime de liquidation du bénéficiaire|||
|PSE_CAT_COD|nombre réel|Catégorie du PS exécutant|||
|BEN_QLT_COD|nombre réel|Qualité du bénéficiaire|||
|PRS_FJH_TYP|nombre réel|Type de prise en charge Forfait Journalier|||
|PRS_DEP_MNT|nombre réel|Montant global du dépassement|||
|ETE_DPT_COD|chaîne de caractères|Département de l'établissement exécutant|||
|PSP_CNV_COD|nombre réel|Code convention du professionnel de santé prescripteur|||
|BEN_RES_DPT|chaîne de caractères|Département de résidence du bénéficiaire|||
|BEN_CTA_TYP|nombre réel|Type de contrat complémentaire|||
|PRS_ACT_NBR|nombre réel|Dénombrement des actes de base|||
|ETE_TYP_COD|nombre réel|Code du type de l'établissement exécutant|||
|TOP_PS5_RG|nombre réel|Top PS5 Régime Général|||
|FLX_EMT_TYP|nombre réel|Type d'émetteur|||
|BEN_NAI_ANN|chaîne de caractères|Année de naissance du bénéficiaire|||
|SOI_MOI|chaîne de caractères|Mois de soin PS5|||
|CPL_COD|nombre réel|Majorations hors actes CCAM - complément dacte|||
|PRS_PAI_MNT|nombre réel|Montant global de la dépense|||
|DPN_QLF|nombre réel|Qualificatif de la dépense|||
|PRS_REM_TYP|nombre réel|Type de remboursement|||
|FLX_ANN_MOI|chaîne de caractères|Année et Mois de traitement|||
|FLX_EMT_NUM|nombre réel|numéro d'émetteur du flux|||
|ATT_NAT|nombre réel|Nature de l'accident du travail|||
|PSP_ACT_NAT|nombre réel|Nature d'activite du professionnel de santé prescripteur|||
|PRS_PPU_SEC|nombre réel|Code privé - public de la Prestations|||
