# EB_RAT_F

Table des données des rentes d'accident du travail


## Modèle de données

|Nom|Type|Description|Exemple|Propriétés|
|-|-|-|-|-|
|CLE_TEC_PRS|chaîne de caractères|Clé technique Prestation|||
|FLX_DIS_DTD|date|Date de mise à disposition des données|||
|BEN_RAT_ANO|chaîne de caractères|N° anonymisé de rente (accident du travail - maladie professionnelle)|||
|REM_TYP_AFF|nombre réel|type de remboursement affiné|||
|ORG_CLE_NUM|chaîne de caractères|organisme de liquidation des prestations (avant fusion des caisses)|||
|ORG_CLE_NEW|chaîne de caractères|Code de l'organisme de liquidation|||
|FLX_TRT_DTD|date|Date d'entrée des données dans le système d'information|||
|FLX_EMT_NUM|nombre réel|numéro d'émetteur du flux|||
|DCT_ORD_NUM|nombre réel|numéro d'ordre du décompte dans l'organisme|||
|PRS_ORD_NUM|nombre réel|Numéro d'ordre de la prestation dans le décompte|||
|FLX_EMT_ORD|nombre réel|numéro de séquence du flux|||
|RAT_IPP_TAU|nombre réel|Taux de l'incapacité permanente (IPP)|||
|ATT_NAT|nombre réel|Nature de l'accident du travail|||
|RAT_IPP_DTE|date|Date d'effet du taux de l'incapacité permanente (IPP)|||
|FLX_EMT_TYP|nombre réel|Type d'émetteur|||
