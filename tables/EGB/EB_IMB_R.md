# EB_IMB_R

Table des données médicalisées des bénéficiaires de l'EGB


## Modèle de données

|Nom|Type|Description|Exemple|Propriétés|
|-|-|-|-|-|
|IMB_MLP_BTR|chaîne de caractères|Tableau des maladies professionnelles (bis, ter)|||
|MED_MTF_COD|chaîne de caractères|Motif médical ou pathologie (code CIM10)|||
|IMB_MLP_TAB|chaîne de caractères|Numéro de tableau des maladies professionnelles (MP)|||
|IMB_ALD_DTD|date|Date de début d'exonération du ticket modérateur attribuée par les services médicaux (ALD, AT, MP)|||
|IMB_ETM_NAT|nombre réel|Motif d'exonération du bénéficiaire|||
|IMB_ALD_NUM|nombre réel|Numéro d'ALD|||
|INS_DTE|date|Date d'insertion|||
|UPD_DTE|date|Date de la dernière sélection des bénéficiaires de l'EGB|||
|IMB_ALD_DTF|date|Date de fin de l'exonération attribuée par les services médicaux (ALD, AT, MP)|||
|BEN_NIR_IDT|chaîne de caractères|NIR anonyme du bénéficiaire dans l'échantillon|||
|MED_NCL_IDT|chaîne de caractères|Nomenclature médicale|||
|IMB_SDR_LOP|chaîne de caractères|Localisation - paragraphe syndrome|||
