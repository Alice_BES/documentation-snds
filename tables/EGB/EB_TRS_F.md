# EB_TRS_F

Table des données détaillées de transport


## Modèle de données

|Nom|Type|Description|Exemple|Propriétés|
|-|-|-|-|-|
|ITR_ORD_NUM|nombre réel|Numéro d'ordre de l'information détaillée du transport|||
|FLX_DIS_DTD|date|Date de mise à disposition des données|||
|TRS_DTF_DTE|date|Date d'arrivée du transport|||
|REM_TYP_AFF|nombre réel|type de remboursement affiné|||
|ORG_CLE_NUM|chaîne de caractères|organisme de liquidation des prestations (avant fusion des caisses)|||
|ORG_CLE_NEW|chaîne de caractères|Code de l'organisme de liquidation|||
|FLX_TRT_DTD|date|Date d'entrée des données dans le système d'information|||
|DCT_ORD_NUM|nombre réel|numéro d'ordre du décompte dans l'organisme|||
|TRS_DTD_AME|chaîne de caractères|Année et mois de départ du transport (format AAAAMM)|||
|FLX_EMT_TYP|nombre réel|Type d'émetteur|||
|TRS_TRP_HRA|nombre réel|Heure d'arrivée du transport|||
|CLE_TEC_PRS|chaîne de caractères|Clé technique Prestation|||
|FLX_EMT_NUM|nombre réel|numéro d'émetteur du flux|||
|PRS_ORD_NUM|nombre réel|Numéro d'ordre de la prestation dans le décompte|||
|TRS_TRP_HRD|nombre réel|Heure de départ du transport|||
|TRS_DTF_AME|chaîne de caractères|Année et mois d'arrivée du transport (format AAAAMM)|||
|FLX_EMT_ORD|nombre réel|numéro de séquence du flux|||
|TRS_DEP_CDP|chaîne de caractères|Code postal du lieu de départ du transport|||
|TRS_ARR_CDP|chaîne de caractères|Code postal du lieu d'arrivée du transport|||
|TRS_DTD_DTE|date|Date de départ du transport|||
