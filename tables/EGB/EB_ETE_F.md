# EB_ETE_F

Table des données des établissements


## Modèle de données

|Nom|Type|Description|Exemple|Propriétés|
|-|-|-|-|-|
|FLX_DIS_DTD|date|Date de mise à disposition des données|||
|ETE_MCO_COE|nombre réel|Coefficient (Non Signé) MCO|||
|REM_TYP_AFF|nombre réel|type de remboursement affiné|||
|ORG_CLE_NUM|chaîne de caractères|organisme de liquidation des prestations (avant fusion des caisses)|||
|DDP_COD|nombre réel|Code de la discipline de Prestations (ou discipline médico tarifaire)|||
|ORG_CLE_NEW|chaîne de caractères|Code de l'organisme de liquidation|||
|FLX_TRT_DTD|date|Date d'entrée des données dans le système d'information|||
|DCT_ORD_NUM|nombre réel|numéro d'ordre du décompte dans l'organisme|||
|ETE_CAT_COD|nombre réel|Catégorie de l'établissement exécutant|||
|ETE_NAT_FSJ|chaîne de caractères|Nature de fin de séjour|||
|MDT_COD|nombre réel|Code du mode de traitement|||
|ETE_TYP_COD|nombre réel|Code du type de l'établissement exécutant|||
|FLX_EMT_TYP|nombre réel|Type d'émetteur|||
|ETB_EXE_FIN|chaîne de caractères|N° FINESS de l'établissement exécutant|||
|ETE_STJ_COD|nombre réel|Statut Juridique de l'établissement juridique|||
|ETE_MCO_DDP|chaîne de caractères|Code discipline MCO établissement exécutant|||
|CLE_TEC_PRS|chaîne de caractères|Clé technique Prestation|||
|ETE_GHS_NUM|nombre réel|Numéro du GHS|||
|ETE_IND_TAA|nombre réel|Indicateur TAA|||
|ETE_ETA_TRF|chaîne de caractères|Numéro d'établissement de transfert|||
|FLX_EMT_NUM|nombre réel|numéro d'émetteur du flux|||
|PRS_ORD_NUM|nombre réel|Numéro d'ordre de la prestation dans le décompte|||
|FLX_EMT_ORD|nombre réel|numéro de séquence du flux|||
|MFT_COD|nombre réel|Code du mode de fixation des tarifs|||
|PRS_PPU_SEC|nombre réel|Code privé - public de la Prestations|||
