# Documents Cnam
<!-- SPDX-License-Identifier: MPL-2.0 -->

Cette section contient une sélection de documents publiés par la [Cnam](../../glossaire/Cnam.md) sur le portail SNDS,
et issus des supports de formation élaborés par la Cnam.

Les documents qui ont été convertis en fiches thématiques sont référencés directement sur chaque fiche.

Note : ce [dossier sur GitLab](https://gitlab.com/healthdatahub/documentation-snds/tree/master/files/Cnam)
contient l'ensemble des documents **et** fiches publiés par la Cnam sur ce site.

